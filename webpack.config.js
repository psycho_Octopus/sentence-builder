const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const resolvePath = p => path.resolve(__dirname, p);

const paths = {
    app: resolvePath('./src'),
    build: resolvePath('./build'),
    devHtml: resolvePath('./src/html/dev.html'),
};

const port = 3456;
const publicPath = '/';
const isEnvDevelopment = process.env.NODE_ENV === 'development';
const isEnvProduction = process.env.NODE_ENV === 'production';

module.exports = {
    mode: isEnvDevelopment ? 'development' : isEnvProduction && 'production',
    devtool: isEnvDevelopment ? 'cheap-module-source-map' : false,
    entry: isEnvDevelopment
        ? [
              'react-hot-loader/patch',
              `webpack-dev-server/client?http://localhost:${port}`,
              'webpack/hot/dev-server',
              paths.app,
          ]
        : paths.app,
    output: {
        path: isEnvProduction ? paths.build : undefined,
        pathinfo: false,
        filename: isEnvProduction
            ? 'static/js/[name].[contenthash:8].js'
            : isEnvDevelopment && 'static/js/bundle.js',
        publicPath,
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json'],
    },
    module: {
        strictExportPresence: true,
        rules: [
            {
                oneOf: [
                    {
                        test: /\.(js|jsx)$/,
                        include: paths.app,
                        loader: require.resolve('babel-loader'),
                        options: {
                            cacheDirectory: true,
                        },
                    },
                    {
                        test: /\.scss$/,
                        include: paths.app,
                        use: [
                            isEnvDevelopment && 'style-loader',
                            isEnvProduction && MiniCssExtractPlugin.loader,
                            'css-loader',
                            'sass-loader',
                        ].filter(Boolean),
                    },
                ],
            },
        ],
    },

    optimization: {
        minimize: isEnvProduction,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    parse: {
                        ecma: 8,
                    },
                    compress: {
                        ecma: 5,
                        warnings: false,
                        comparisons: false,
                        inline: 2,
                    },
                    mangle: true,
                    output: {
                        ecma: 5,
                        comments: false,
                        ascii_only: true,
                    },
                },
                parallel: true,
                cache: true,
                sourceMap: false,
            }),
            new OptimizeCSSAssetsPlugin(),
        ],
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    chunks: 'initial',
                    name: 'vendor',
                    enforce: true,
                },
            },
        },
    },
    plugins: isEnvDevelopment
        ? [
              new HtmlWebpackPlugin({
                  inject: false,
                  template: paths.devHtml,
              }),
              new webpack.HotModuleReplacementPlugin(),
          ]
        : [
              new CleanWebpackPlugin({
                  cleanStaleWebpackAssets: true,
              }),
              new HtmlWebpackPlugin({
                  inject: false,
                  template: paths.devHtml,
                  minify: {
                      removeComments: true,
                      collapseWhitespace: false,
                      removeRedundantAttributes: true,
                      useShortDoctype: true,
                      removeEmptyAttributes: true,
                      removeStyleLinkTypeAttributes: true,
                      keepClosingSlash: true,
                      //   minifyJS: true,
                      minifyCSS: true,
                      minifyURLs: true,
                  },
              }),
              new MiniCssExtractPlugin({
                  filename: 'static/css/[name].[contenthash:8].css',
                  chunkFilename: 'static/css/[name].[contenthash:8].chunk.css',
              }),
          ],
    devServer: {
        port,
        host: 'localhost',
        hot: true,
    },
};
