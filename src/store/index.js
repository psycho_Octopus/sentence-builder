import { createStore, applyMiddleware, compose, combineReducers } from 'redux';

import * as builder from './builder';

const rootReducer = combineReducers({
    [builder.KEY]: builder.reducer,
});

export default ({ enableDevTools } = {}) => {
    const enhancers = [];

    if (enableDevTools) {
        // eslint-disable-next-line no-underscore-dangle
        const reduxDevToolExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

        if (reduxDevToolExtension) {
            enhancers.push(reduxDevToolExtension());
        }
    }

    const store = createStore(
        rootReducer,
        compose(
            applyMiddleware(),
            ...enhancers,
        ),
    );

    return store;
};
