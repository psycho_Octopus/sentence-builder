import * as R from 'ramda';

import { makeReducer, makeActionCreator, makeActionTypes } from '../libs/redux';

export const KEY = 'builder';

const defaultState = {
    index: 0,
    isComplete: false,
    questions: [
        {
            id: '__WHO__',
            question: 'Who?',
            answer: '',
            isValid: true,
        },
        {
            id: '__WHAT__',
            question: 'What?',
            answer: '',
            isValid: true,
        },
        {
            id: '__WHEN__',
            question: 'When?',
            answer: '',
            isValid: true,
        },
        {
            id: '__WHERE__',
            question: 'Where?',
            answer: '',
            isValid: true,
        },
    ],
};

// #region ACTIONS
const actionTypes = makeActionTypes(KEY, ['ANSWER', 'RESET']);

export const actions = {
    answer: makeActionCreator(actionTypes.ANSWER, ['answer']),
    reset: makeActionCreator(actionTypes.RESET),
};
// #endregion

// #region REDUCER
const bumpUpIndex = R.over(R.lensProp('index'), R.inc);
// Set answer of the current question
const setAnswer = answer =>
    R.converge(R.set, [
        R.compose(
            R.lensPath,
            R.update(1, R.__, ['questions', 0, 'answer']),
            R.prop('index'),
        ),
        R.always(R.trim(answer)),
        R.identity,
    ]);
const toogleAnswerValidity = isValid =>
    R.converge(R.set, [
        R.compose(
            R.lensPath,
            R.update(1, R.__, ['questions', 0, 'isValid']),
            R.prop('index'),
        ),
        R.always(isValid),
        R.identity,
    ]);
const complete = R.assoc('isComplete', true);
// Turn all answers into empty value
const resetAnswers = R.over(R.lensProp('questions'), R.map(R.assoc('answer', '')));
const isAnswerValid = R.allPass([
    R.propSatisfies(R.gte(R.__, 3), 'length'), // Min length
    R.propSatisfies(R.lte(R.__, 100), 'length'), // Max length
    R.complement(R.test(/[^a-z\sA-Z0-9]+/g)), // Valid symbols
]);
// Question is last when index + 1 === questions.length
const isLastQuestion = R.converge(R.equals, [
    R.compose(
        R.inc,
        R.prop('index'),
    ),
    R.path(['questions', 'length']),
]);

export const reducer = makeReducer(defaultState, {
    [actionTypes.ANSWER]: ({ answer }) =>
        R.ifElse(
            R.always(isAnswerValid(answer)),
            R.compose(
                R.ifElse(isLastQuestion, complete, bumpUpIndex),
                toogleAnswerValidity(true),
                setAnswer(answer),
            ),
            toogleAnswerValidity(false),
        ),
    [actionTypes.RESET]: () =>
        R.compose(
            R.assoc('isComplete', false),
            R.assoc('index', 0),
            resetAnswers,
        ),
});
// #endregion

// #region SELECTORS
const get = R.prop(KEY);

export const selectors = {
    getCurrQuestion: R.compose(
        R.defaultTo(null),
        R.converge(R.nth, [R.prop('index'), R.prop('questions')]),
        get,
    ),
    isLastQuestion: R.compose(
        isLastQuestion,
        get,
    ),
    isAnswerValid: R.compose(
        R.converge(R.view, [
            R.compose(
                R.lensPath,
                R.update(1, R.__, ['questions', 0, 'isValid']),
                R.prop('index'),
            ),
            R.identity,
        ]),
        get,
    ),
    isComplete: R.compose(
        R.prop('isComplete'),
        get,
    ),
    getSentence: ids => state => {
        const questions = R.compose(
            R.prop('questions'),
            get,
        )(state);

        return R.compose(
            R.join(' '),
            R.pluck('answer'),
            R.map(id => R.find(R.propEq('id', id), questions)),
        )(ids);
    },
};
// #endregion
