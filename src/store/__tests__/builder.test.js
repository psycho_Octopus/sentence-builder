import * as R from 'ramda';
import { KEY, actions, reducer, selectors } from '../builder';

const createLocalState = R.mergeRight({
    index: 0,
    isComplete: false,
    questions: [{ question: 'Who?', answer: '' }, { question: 'Why?', answer: '' }],
});

const createState = localState => ({
    [KEY]: localState,
});

describe('builder', () => {
    describe('actions', () => {
        it('should create an action to answer', () => {
            const answer = 'test';
            const action = actions.answer(answer);

            expect(action).toEqual({
                type: 'builder/ANSWER',
                answer,
            });
        });

        it('should create an action to reset', () => {
            const action = actions.reset();

            expect(action).toEqual({
                type: 'builder/RESET',
            });
        });
    });

    describe('reducer', () => {
        describe('ANSWER', () => {
            describe('if an answer is valid', () => {
                it('should update answer', () => {
                    const answer = 'valid answer';
                    const index = 0;
                    const localState = createLocalState({
                        index,
                        questions: [{ answer: '' }, { answer: '' }],
                    });
                    const action = actions.answer(answer);
                    const result = reducer(localState, action);

                    expect(result.index).toBe(index + 1);
                    expect(result.isComplete).toBeFalse();
                    expect(result.questions[index].answer).toBe(answer);
                });
                it('should mark as completed if the asnwer is last', () => {
                    const answer = 'valid answer';
                    const index = 1;
                    const localState = createLocalState({
                        index,
                        questions: [{ answer: '' }, { answer: '' }],
                    });
                    const action = actions.answer(answer);
                    const result = reducer(localState, action);

                    expect(result.index).toBe(index);
                    expect(result.isComplete).toBeTrue();
                });
            });
            describe('if an answer is invalid', () => {
                it('should not update answer if one is invalid', () => {
                    // TODO: add test for an answer longer than 100
                    ['sh', '<daw', 'BOb>'].forEach(answer => {
                        const index = 0;
                        const localState = createLocalState({
                            index,
                            questions: [{ answer: '' }, { answer: '' }],
                        });
                        const action = actions.answer(answer);
                        const result = reducer(localState, action);

                        expect(result.index).toBe(index);
                        expect(result.questions[index].answer).toBe('');
                    });
                });
            });

            it('should mark the answer is invalid if one is invalid', () => {
                const answer = 'x';
                const index = 0;
                const localState = createLocalState({
                    index,
                    questions: [{ answer: '' }, { answer: '' }],
                });
                const action = actions.answer(answer);
                const result = reducer(localState, action);

                expect(result.questions[index].isValid).toBe(false);
            });
        });
    });

    describe('selectors', () => {
        describe('getCurrQuestion', () => {
            it('should return current question if one exists', () => {
                const questions = [0, 1, 2].map(id => ({
                    id,
                }));

                [0, 1, 2].forEach(index => {
                    const localState = createLocalState({
                        index,
                        questions,
                    });
                    const state = {
                        [KEY]: localState,
                    };
                    const result = selectors.getCurrQuestion(state);

                    expect(result).toMatchObject({
                        id: index,
                    });
                });
            });

            it('should return null if there is no questions', () => {
                const localState = createLocalState({
                    questions: [],
                });
                const state = createState(localState);
                const result = selectors.getCurrQuestion(state);

                expect(result).toBeNil();
            });
        });
        describe('isLastQuestion', () => {
            it('should return "true" if current question is last', () => {
                const localState = createLocalState({
                    index: 1,
                    questions: [{}, {}],
                });
                const state = createState(localState);
                const result = selectors.isLastQuestion(state);

                expect(result).toBeTrue();
            });
            it('should return "false" if current question is not last', () => {
                const localState = createLocalState({
                    index: 0,
                    questions: [{}, {}],
                });
                const state = createState(localState);
                const result = selectors.isLastQuestion(state);

                expect(result).toBeFalse();
            });
        });
        describe('isAnswerValid', () => {
            it('should return "true" if the answer is valid', () => {
                const localState = createLocalState({
                    index: 0,
                    questions: [{ isValid: true }],
                });
                const state = createState(localState);
                const result = selectors.isAnswerValid(state);

                expect(result).toBeTrue();
            });
            it('should return "true" if the answer is not valid', () => {
                const localState = createLocalState({
                    index: 0,
                    questions: [{ isValid: false }],
                });
                const state = createState(localState);
                const result = selectors.isAnswerValid(state);

                expect(result).toBeFalse();
            });
        });
        describe('isComplete', () => {
            it('should return "isComplete" value', () => {
                [true, false].forEach(isComplete => {
                    const localState = {
                        isComplete,
                    };
                    const state = createState(localState);
                    const result = selectors.isComplete(state);

                    expect(result).toBe(isComplete);
                });
            });
        });
        describe('getSentence', () => {
            it('should return sentence', () => {
                [
                    {
                        questions: [
                            { answer: 'Bob' },
                            { answer: 'tomorrow' },
                            { answer: 'moscow' },
                        ],
                        sentence: 'Bob tomorrow moscow',
                    },
                    {
                        questions: [{ answer: 'Bob' }],
                        sentence: 'Bob',
                    },
                    {
                        questions: [],
                        sentence: '',
                    },
                ].forEach(({ questions, sentence }) => {
                    const localState = createLocalState({
                        index: 0,
                        questions,
                    });
                    const state = createState(localState);
                    const result = selectors.getSentence(state);

                    expect(result).toBe(sentence);
                });
            });
        });
    });
});
