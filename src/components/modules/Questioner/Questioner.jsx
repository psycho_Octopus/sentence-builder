import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import bemCl from 'bem-cl';

import Input from '../../base/Input';
import Button from '../../base/Button';
import * as builder from '../../../store/builder';

import './Questioner.scss';

const b = bemCl('questioner');

const Questioner = () => {
    const currQuestion = useSelector(builder.selectors.getCurrQuestion);
    const dispatch = useDispatch();
    const isLastQuestion = useSelector(builder.selectors.isLastQuestion);
    const isAnswerValid = useSelector(builder.selectors.isAnswerValid);
    const [answer, setAnswer] = useState(currQuestion.answer);

    useEffect(() => {
        setAnswer(currQuestion.answer);
    }, [currQuestion.question]);

    if (!currQuestion) {
        return null;
    }

    const handleInputChange = event => {
        const { value } = event.target;

        setAnswer(value);
    };

    const handleFormSubmit = e => {
        e.preventDefault();

        dispatch(builder.actions.answer(answer));
    };

    return (
        <form onSubmit={handleFormSubmit}>
            {/* QUESTION */}
            <label className={b('question')} htmlFor="questioner-item-input">
                Question: {currQuestion.question}
            </label>

            {/* INPUT */}
            <Input
                value={answer}
                className={b('input').toString()}
                id="questioner-item-input"
                state={isAnswerValid ? 'normal' : 'invalid'}
                placeholder="your answer"
                onChange={handleInputChange}
                autoFocus
                key={currQuestion.question}
            />

            {/* CONTROLS */}
            <div className={b('controls')}>
                {isLastQuestion ? (
                    <Button type="submit">Complete</Button>
                ) : (
                    <Button type="submit">Answer</Button>
                )}
            </div>
        </form>
    );
};

export default Questioner;
