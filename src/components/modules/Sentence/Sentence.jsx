import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import bemCl from 'bem-cl';

import * as builder from '../../../store/builder';
import Button from '../../base/Button';

import './Sentence.scss';

const b = bemCl('sentence');

const Sentence = () => {
    const dispatch = useDispatch();

    const sentence = useSelector(
        builder.selectors.getSentence(['__WHO__', '__WHAT__', '__WHERE__', '__WHEN__']),
    );

    const handleAgainButtonClick = () => {
        dispatch(builder.actions.reset());
    };

    return (
        <div className={b()}>
            <div>{sentence}</div>
            <Button className={b('reset-button').toString()} onClick={handleAgainButtonClick}>
                Again
            </Button>
        </div>
    );
};

export default Sentence;
