import React from 'react';
import { useSelector } from 'react-redux';
import bemCl from 'bem-cl';

import Questioner from '../modules/Questioner';
import Sentence from '../modules/Sentence';
import * as builder from '../../store/builder';

import './Application.scss';

const b = bemCl('application');

const Application = () => {
    const isBuilderComplete = useSelector(builder.selectors.isComplete);

    return (
        <div className={b()}>
            <h1 className={b('title')}>Sentence Builder</h1>
            {!isBuilderComplete ? <Questioner /> : <Sentence />}
        </div>
    );
};

export default Application;
