import React from 'react';
import { string, oneOf } from 'prop-types';
import bemCl from 'bem-cl';

import './Input.scss';

const b = bemCl('input');

const Item = ({ className, state, ...htmlProps }) => {
    return <input className={b({ state }).mix(className)} {...htmlProps} />;
};

Item.propTypes = {
    className: string,
    state: oneOf(['normal', 'invalid']),
};

Item.defaultProps = {
    className: '',
    state: 'normal',
};

export default Item;
