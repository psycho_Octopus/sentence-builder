import React from 'react';
import { string, any } from 'prop-types';
import bemCl from 'bem-cl';

import './Button.scss';

const b = bemCl('button');

const Item = ({ children, className, ...htmlProps }) => {
    return (
        <button className={b().mix(className)} type="button" {...htmlProps}>
            {children}
        </button>
    );
};

Item.propTypes = {
    children: any.isRequired,
    className: string,
};

Item.defaultProps = {
    className: '',
};

export default Item;
